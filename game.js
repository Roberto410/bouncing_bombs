var bombList = [];
var pauseGame = false;
var gameScore = 0;
var vCanvas;

function setup() {
    vCanvas = createCanvas(500, 500);
    vCanvas.parent('gameArea');
    let numStartingBombs = 5;

    for (i = 0; i < numStartingBombs; i++) {
        let tempBomb = new Bomb();
        bombList.push(tempBomb);
    }
}

function draw() {

    if(bombList.length > 200) {
        pauseGame = true;
    }

    if (!pauseGame) {
    background(0);

    
        let numBombToLoop = bombList.length;
        for (i = 0; i < numBombToLoop; i++) {
            
                bombList[i].moveBomb();
                if (mouseIntersectsBomb(bombList[i])) {
                    pauseGame = !pauseGame;
                    alert("Game Over! Your Score was: " + gameScore);
                }
            

            bombList[i].show();

            if (bombList[i].shouldDie()) {
                let bombToDie = bombList[i];
                bombList.splice(i, 1);
                createTwoNewBombs(bombToDie);
                gameScore += bombToDie.score;
            }
        }
    }

    fill(255);
    ellipse(mouseX, mouseY, 15);
}

function mouseIntersectsBomb(bomb) {
    if (bomb.contains(mouseX, mouseY)) {
        return true;
    } else {
        return false;
    }
}

function createTwoNewBombs(oldBomb) {
    for (i = 0; i < 2; i++) {
        let tempBomb = new Bomb(oldBomb.xPos, oldBomb.yPos);

        print(tempBomb.maxBounces);
        bombList.push(tempBomb);
    }
}

function keyPressed() {
    if (keyCode === ENTER) {
        pauseGame = !pauseGame;
  }
}



class Bomb {



    
    constructor(xP = int(random(21,width-21)), yP = int(random(21,height-21)), rP = int(random(5,20))) {
        this.radius = rP;
        this.numBounces = 0;
        this.maxBounces = int(random(3,10));
        this.diameter = this.radius * 2;
        this.xPos = xP;
        this.yPos = yP;
        this.color = color(int(random(0,255)), int(random(0,255)), int(random(0,255)));
        this.movementDirections = [random(-1, 0, 1), random(-1, 0, 1)];
        this.speed = int(random(3,8));
        this.BouncedOffx = false;
        this.BouncedOffy = false;
        this.score = int(this.radius / 10 * this.speed); 
    }

    show() {
        fill(this.color);
        ellipse(this.xPos, this.yPos, this.diameter); 
    }

    contains(px, py) {
        
        let d = dist(px, py, this.xPos, this.yPos);
        if (d < this.radius) {
            return true;
        } else {
            return false;
        }
    }

    moveBomb() {
        //print(this.xPos);
        if (this.xPos < (this.radius)) {
            this.toggleMovementDirection(0);
            this.xPos = this.radius;
            this.BouncedOffx = true;
        }
        if (this.xPos > (width - this.radius)) {
            this.toggleMovementDirection(0);
            this.xPos = width - this.radius;
            this.BouncedOffx = true;
        }
        if (this.yPos < (this.radius)) {
            this.toggleMovementDirection(1);
            this.yPos = this.radius;
            this.BouncedOffy = true;
        }
        if (this.yPos > (height- this.radius)) {
            this.toggleMovementDirection(1);
            this.yPos = height - this.radius;
            this.BouncedOffy = true;
        }
        this.xPos += this.movementDirections[0] * this.speed;
        this.yPos += this.movementDirections[1] * this.speed;
    }

    toggleMovementDirection(directionInList) {
        this.movementDirections[directionInList] *= -1;
        this.numBounces++;
    }

    shouldDie() {
        if (this.numBounces > this.maxBounces && this.BouncedOffx && this.BouncedOffy) {
            return true;
        }
        else {
            return false;
        }
    }
}